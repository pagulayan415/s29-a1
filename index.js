const express = require('express');

const app = express();

const port = 3000;

let users = [];

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get('/home', (req, res)=>{
	res.send('This is home')
})

app.get('/users', (req, res)=>{
	res.send(users)
})

app.post('/users', (req, res)=>{
	users.push(req.body)
	res.send(`User "${req.body.firstName} ${req.body.lastName}" successfuly added`)
})




app.listen(port, ()=>{
	console.log(`Server running at port ${port}`)
})